# How We Survive in China

![Banner](https://raw.githubusercontent.com/panteng/how-we-survive-in-china/master/images/banner.jpg)

A list of tools to make you live more sadly in China.


## VPNs or Proxies I've used:

 - [ExpressVPN](https://www.expressvpn.com/)

 - [AstrillVPN](https://www.astrill.com/)

 - [EurekaVPT](https://eurekavpt.com/)
 
 - [Lantern](https://getlantern.org/)


## Socks5 Proxy Tools:

 - [Shadowsocks](https://shadowsocks.org/)
 
 
## Shadowsocks Service Providers:

 - [Shadowsocks.com](https://shadowsocks.com/)

 - [DeepSS](https://www.deepss.org/)
 
 
## Proxy Managers For Chrome

 - [Proxy SwitchyOmega](https://chrome.google.com/webstore/detail/proxy-switchyomega/padekgcemlokbadohgkifijomclgjgif)
 
 
## DNS Quick Switch

 - [DNS Jumper](http://www.sordum.org/7952/dns-jumper-v2-0/)
 
 
## Tools that Convert Socks or HTTP(S) Proxies into Global Proxies

 - [Proxifier](https://www.proxifier.com/)
 
 
## NPM Mirror For China

- [TAONPM](https://npm.taobao.org/)